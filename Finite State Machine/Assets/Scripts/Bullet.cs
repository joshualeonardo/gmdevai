﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
	public int damage;

	void OnCollisionEnter(Collision col)
    {
		if(col.gameObject.GetComponent<TankAI>()) {
			col.gameObject.GetComponent<TankAI>().health -= damage;
		}

		if(col.gameObject.GetComponent<Drive>()) {
			col.gameObject.GetComponent<Drive>().health -= damage;
		}
    	GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
    	Destroy(e,1.5f);
    	Destroy(this.gameObject);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
