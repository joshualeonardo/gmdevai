﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelForwardToGoal : MonoBehaviour{

    public Transform goal;
    public float timeToLerp = 1.0f;
    public float speed = 1;
    public float maxSpeed = 3;
    public float acceleration = 3;
    public float deceleration = 3;
    public float rotSpeed = 4;

    Vector3 startPosition;
    Vector3 endPosition;
    float timeStartedLerping;

    // Start is called before the first frame update
    void Start() {
        startPosition = transform.position;
        timeStartedLerping = Time.time;
    }

    // Update is called once per frame
    void Update() {

        Vector3 lookAtGoal = new Vector3(goal.position.x, 
                                         this.transform.position.y, 
                                         goal.position.z); 
   
        Vector3 direction = lookAtGoal - transform.position; 
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                   Quaternion.LookRotation(direction),
                                                   Time.deltaTime * rotSpeed);


        endPosition = lookAtGoal + Vector3.forward * speed;
        float timeSinceStarted = Time.time - timeStartedLerping;
        float percentageComplete = timeSinceStarted / timeToLerp;

       
        if(Vector3.Distance(lookAtGoal,transform.position) >= 3.0f) {
            transform.position = Vector3.Lerp(startPosition, endPosition, percentageComplete);
        }
    }
}
