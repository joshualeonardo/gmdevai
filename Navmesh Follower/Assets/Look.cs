﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//source: https://www.youtube.com/watch?v=_QajrabyTJc
public class Look : MonoBehaviour
{
    public float sensivity = 100f;
    public Transform player;
    private float xRotation = 0;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mousePositionX = Input.GetAxis("Mouse X") * sensivity * Time.deltaTime;
        float mousePositionY = Input.GetAxis("Mouse Y") * sensivity * Time.deltaTime;
        
        xRotation -= mousePositionY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        player.Rotate(Vector3.up * mousePositionX);

    }
}
